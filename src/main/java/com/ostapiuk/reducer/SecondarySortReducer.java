package com.ostapiuk.reducer;

import com.ostapiuk.utils.CompositeKey;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class SecondarySortReducer extends Reducer<CompositeKey, Text,Text, IntWritable> {

    protected void reduce(CompositeKey key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
        int count = 0;
        String name = null;
        for(Text value: values) {
            if ( !value.toString().matches("[0-9]") ) {
                name = value.toString();
            } else if (value.toString().matches("[0-9]")) {
                count += Integer.parseInt(value.toString());
            }
        }
        if(count!=0) context.write(new Text(name),new IntWritable(count));
    }
}
