package com.ostapiuk.mapper;

import com.ostapiuk.utils.CompositeKey;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SecondarySortMapper extends Mapper<LongWritable, Text, CompositeKey, Text> {
    CompositeKey compositeKey;

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        if (!line.isEmpty()) {
            String[] values = line.trim().split(" ");
            String hotelId = values[19];
            String srch_ci = values[12];
            String bookingId = values[0];
            context.write(new CompositeKey(hotelId, srch_ci, bookingId), new Text("1"));
        }
    }
}
