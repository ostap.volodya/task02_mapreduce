package com.ostapiuk;

import com.ostapiuk.mapper.SecondarySortMapper;
import com.ostapiuk.partitioner.SecondarySortPartitioner;
import com.ostapiuk.reducer.SecondarySortReducer;
import com.ostapiuk.utils.CompositeKey;
import com.ostapiuk.utils.GroupingComparator;
import com.ostapiuk.utils.SortingComparator;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class SecondarySortDriver extends Configured implements Tool {

    public int run (String[] args) throws Exception {

        if (args.length != 3) {
            System.err.printf("Usage: %s <inputFolder> <outputFolder>\n", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration, "Secondary Sorting");
        job.setJarByClass(SecondarySortDriver.class);

        // input paths
        Configuration conf = new Configuration();
        FileSystem fs= FileSystem.get(conf);
        FileInputFormat.setInputDirRecursive(job,true);
        FileStatus[] status_list = fs.listStatus(new Path(args[0]));
//        FileStatus[] status_list = fs.listStatus(new Path("src/main/resources/input"));
        if(status_list != null){
            for(FileStatus status : status_list){
                MultipleInputs.addInputPath(job, status.getPath(), TextInputFormat.class, SecondarySortMapper.class);
            }
        }

        FileOutputFormat.setOutputPath(job, new Path(args[1]));
//        FileOutputFormat.setOutputPath(job, new Path("src/main/resources/output"));

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setCombinerKeyGroupingComparatorClass(GroupingComparator.class);
        job.setPartitionerClass(SecondarySortPartitioner.class);
        job.setGroupingComparatorClass(GroupingComparator.class);
        job.setSortComparatorClass(SortingComparator.class);
        job.setReducerClass(SecondarySortReducer.class);
        job.setMapOutputKeyClass(CompositeKey.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setNumReduceTasks(4);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {

        int res = ToolRunner.run(new Configuration(), new SecondarySortDriver(), args);
        System.exit(res);
    }
}
