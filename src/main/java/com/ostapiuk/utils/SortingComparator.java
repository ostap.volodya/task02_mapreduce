package com.ostapiuk.utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class SortingComparator extends WritableComparator {
    protected SortingComparator() {
        super(CompositeKey.class, true);
    }

    @Override
    public int compare(WritableComparable writable1, WritableComparable writable2) {
        CompositeKey firstKey = (CompositeKey) writable1;
        CompositeKey secondKey = (CompositeKey) writable2;
        if (firstKey.getHotelId().compareTo(secondKey.getHotelId()) == 0) {
            if (firstKey.getSrch_ci().compareTo(secondKey.getSrch_ci()) != 0) {
                return -firstKey.getSrch_ci().compareTo(secondKey.getSrch_ci());
            } else if (firstKey.getBookingId().compareTo(secondKey.getBookingId()) != 0) {
                return -firstKey.getBookingId().compareTo(secondKey.getBookingId());
            } else
                return 0;
        }
        return firstKey.getHotelId().compareTo(secondKey.getHotelId());
    }
}