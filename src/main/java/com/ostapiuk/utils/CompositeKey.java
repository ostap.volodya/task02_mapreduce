package com.ostapiuk.utils;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class CompositeKey implements Writable, WritableComparable<CompositeKey> {
    private String hotelId;
    private String srch_ci;
    private String bookingId;

    @Override
    public String toString() {
        return "CompositeKey{" +
                "hotelId='" + hotelId + '\'' +
                ", srch_ci='" + srch_ci + '\'' +
                ", bookingId='" + bookingId + '\'' +
                '}';
    }

    public CompositeKey() {
        this.hotelId = "";
        this.srch_ci = "";
        this.bookingId = "";
    }

    public CompositeKey(String hotelId) {
        super();
        this.hotelId = hotelId;
    }

    public CompositeKey(String hotelId, String srch_ci, String bookingId) {
        this.hotelId = hotelId;
        this.srch_ci = srch_ci;
        this.bookingId = bookingId;
    }

    @Override
    public int compareTo(CompositeKey compositeKey) {
        if (compositeKey != null && compositeKey.getHotelId() != null && this.hotelId.compareTo(compositeKey.getHotelId()) != 0) {
            return this.hotelId.compareTo(compositeKey.getHotelId());
        } else if (compositeKey != null && compositeKey.getSrch_ci() != null && this.srch_ci.compareTo(compositeKey.getSrch_ci()) != 0) {
            return this.srch_ci.compareTo(compositeKey.getSrch_ci());
        } else if (compositeKey != null && compositeKey.getBookingId() != null && this.bookingId.compareTo(compositeKey.getBookingId()) != 0) {
            return this.bookingId.compareTo(compositeKey.getBookingId());
        } else {
            return 0;
        }
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getSrch_ci() {
        return srch_ci;
    }

    public void setSrch_ci(String srch_ci) {
        this.srch_ci = srch_ci;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.getHotelId());
        dataOutput.writeUTF(this.getSrch_ci());
        dataOutput.writeUTF(this.getBookingId());
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.hotelId = dataInput.readUTF();
        this.srch_ci = dataInput.readUTF();
        this.bookingId = dataInput.readUTF();
    }

    @Override
    public int hashCode() {
        return this.hotelId.hashCode();
    }
}
