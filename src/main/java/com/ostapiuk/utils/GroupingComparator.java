package com.ostapiuk.utils;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupingComparator extends WritableComparator {
    protected GroupingComparator() {
        super(CompositeKey.class, true);
    }

    @Override
    public int compare(WritableComparable writable1, WritableComparable writable2) {
        CompositeKey firstKey = (CompositeKey) writable1;
        CompositeKey secondKey = (CompositeKey) writable2;
        return firstKey.getHotelId().compareTo(secondKey.getHotelId());
    }
}