package com.ostapiuk.partitioner;

import com.ostapiuk.utils.CompositeKey;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class SecondarySortPartitioner extends Partitioner<CompositeKey, Text> {

    @Override
    public int getPartition(CompositeKey key, Text value, int numPartitions) {
        return (key.getHotelId().hashCode() % numPartitions);
    }
}
